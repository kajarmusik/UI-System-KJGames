using UnityEngine;

namespace KJGames.UI
{ 
    public class UIBaseView : MonoBehaviour
    {
        #region Serialized Fields
        [SerializeField] private Canvas canvas;
        #endregion

        #region Virtual Methods
        public virtual void Show()
        {
            canvas.enabled = true;
        }
        public virtual void Hide()
        {
            canvas.enabled = false;
        }
        public virtual void Init() { }
        public virtual void DeInit() { }
        public virtual void UpdateView() { }
        #endregion
    }
}