using UnityEngine;

namespace KJGames.UI
{
    public class StateMachine : MonoBehaviour
    {
        private BaseState currentState;
        public BaseState CurrentState => currentState;

        [SerializeField] private UIRoot ui;
        public UIRoot UI => ui;


        private void OnDisable()
        {
            currentState.DestroyState();
        }

        private void Update()
        {
            if (currentState != null)
            {
                currentState.UpdateState();
            }
        }

        public void ChangeState(BaseState newState)
        {
            if (currentState != null)
            {
                currentState.DestroyState();
            }

            currentState = newState;

            if (currentState != null)
            {
                currentState.Owner = this;
                currentState.PrepareState();
            }
        }
    }
}

