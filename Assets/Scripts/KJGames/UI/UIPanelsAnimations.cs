using System.Collections;
using UnityEngine;

namespace KJGames.UI
{
    public class UIPanelsAnimations
    {
        private const float CONST_panelsAnimationsMaxTime = 0.3f;

        #region Fields - Fade
        private const float finalAlphaOnOpen = 1.0f;
        private const float finalAlphaOnClose = 0.0f;
        #endregion

        #region Fields - Scale
        private Vector3 finalScaleOnOpen = Vector3.one;
        private Vector3 finalScaleOnHide = Vector3.zero;
        #endregion

        #region Punch Animation
        private WaitForSeconds waitForOtherAnim = new WaitForSeconds(0.2f);
        #endregion

        #region Punch Animations
 
        #endregion

        #region Scale Animations
        private IEnumerator OpenScaleAnimation(Transform panelTransform) 
        {
            float time = 0;

            while (time < CONST_panelsAnimationsMaxTime) 
            {
                time += Time.deltaTime;

                panelTransform.localScale = Vector3.Lerp(finalScaleOnHide, finalScaleOnOpen, time / CONST_panelsAnimationsMaxTime); 

                yield return null;
            }

            panelTransform.localScale = finalScaleOnOpen;
        }
        private IEnumerator HideScaleAnimation(Transform panelTransform)
        {
            float time = 0;

            while (time < CONST_panelsAnimationsMaxTime)
            {
                time += Time.deltaTime;

                panelTransform.localScale = Vector3.Lerp(finalScaleOnOpen, finalScaleOnHide, time / CONST_panelsAnimationsMaxTime);

                yield return null;
            }

            panelTransform.localScale = finalScaleOnHide;
        }
        #endregion

        #region Fade Animations
        private IEnumerator OpenFadeAnimation(CanvasGroup panelCanvasGroup)
        {
            float time = 0;

            while (time < CONST_panelsAnimationsMaxTime)
            {
                time += Time.deltaTime;

                panelCanvasGroup.alpha = Mathf.Lerp(0.0f, finalAlphaOnOpen, time / CONST_panelsAnimationsMaxTime);

                yield return null;
            }

            panelCanvasGroup.alpha = finalAlphaOnOpen;
        }
        #endregion
    }
}
