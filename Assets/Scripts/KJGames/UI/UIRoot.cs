using UnityEngine;

namespace KJGames.UI
{ 
    public class UIRoot : MonoBehaviour
    {
        #region Serialized Fields - Panels
        [SerializeField] private UIBaseView [] views;
        #endregion

        public T GetView<T>() where T : UIBaseView 
        {
            // TODO add dictionary<System.Type, UIBaseView>
            for (int i = 0; i < views.Length; i++)
            {
                if (views[i] is T)
                {
                    return (T)views[i];
                }
            }
            return default(T);
        }
    }
}
