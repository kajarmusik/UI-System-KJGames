using System;

namespace KJGames.UI
{
    public abstract class DataModel {  }

    public class UIPanelView : UIBaseView
    {
        protected DataModel DataModel;
        public void SetData(DataModel data)
        {
            DataModel = data;
        }
    } 
}
