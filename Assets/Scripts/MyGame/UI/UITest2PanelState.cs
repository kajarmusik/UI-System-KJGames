using KJGames.UI;

namespace MyGame.UI
{ 
    public class UITest2PanelState : BaseState
    {
        public UITest2PanelState(BaseState previousState) : base(previousState)
        {
            PreviousState = previousState;
        }

        UITest2PanelView view;

        public override void PrepareState()
        {
            base.PrepareState();

            view = Owner.UI.GetView<UITest2PanelView>();
            view.Init();
            view.Show();
        }
    }
}
