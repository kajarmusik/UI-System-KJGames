using KJGames.UI;

namespace MyGame.UI
{
    public class UITestPanelState : BaseState
    {
        public UITestPanelState(BaseState previousState) : base(previousState)
        {
            PreviousState = previousState;
        }

        private UITestPanelView view;

        private const string CONST_TestString = "Hello";

        public override void PrepareState()
        {
            base.PrepareState();

            GetData();

            view = Owner.UI.GetView<UITestPanelView>();
            CreateTestPanelDataModel();

            view.Init();
            view.Show();
        }

        public override void DestroyState()
        {
            base.DestroyState();

            view.DeInit();
            view.Hide();
        }

        private void GetData()
        {
            //TODO
        }

        private void CreateTestPanelDataModel()
        {
            UITestPanelDataModel testPanelDataModel = new UITestPanelDataModel()
            {
                OnConfirm = Confirm,
                IsConfirmActive = true,
                TestString = CONST_TestString
            };

            view.SetData(testPanelDataModel);
        }

        private void Confirm()
        {
            UnityEngine.Debug.LogError("Confirm");
        }
    } 
}
