using UnityEngine;
using KJGames.UI;

namespace MyGame.UI
{
    public class UIMainSceneController : MonoBehaviour
    {
        [SerializeField] private StateMachine stateMachine;

        private void Start()
        {
            // CreateTestPanelDataModel();
            //  uIRoot.UITestPanelView.Init();
        }


        public void OpenTestPanel()
        {
            stateMachine.ChangeState(new UITestPanelState(stateMachine.CurrentState));
        }

        public void OpenTest2Panel()
        {
            stateMachine.ChangeState(new UITest2PanelState(stateMachine.CurrentState));
        }
    }
}
