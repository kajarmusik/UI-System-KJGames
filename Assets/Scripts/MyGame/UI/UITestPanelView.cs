using UnityEngine;
using UnityEngine.UI;
using KJGames.UI;
using TMPro;
using System;

namespace MyGame.UI
{ 
    public class UITestPanelDataModel : DataModel
    {
        public string TestString { get; set; }
        public bool IsConfirmActive { get; set; }
        public Action OnConfirm;
    }

    public class UITestPanelView : UIPanelView
    {
        #region Serialized Fields
        [SerializeField] private TextMeshProUGUI HeaderLabel;
        [SerializeField] private Button ConfirmButton;
        [SerializeField] private Image Fill;
        #endregion

        UITestPanelDataModel dataModel => (UITestPanelDataModel)DataModel;

        #region  Public Methods
        public override void Init()
        {
            base.Init();

            ChangeTextColor(Color.white);
        }

        public override void Show()
        {
            base.Show();

            UpdateView();
        }
        public override void UpdateView()
        {
            base.UpdateView();

            HeaderLabel.text = dataModel.TestString;
            ConfirmButton.interactable = dataModel.IsConfirmActive;
        }
        public void OnBtnConfirmClicked()
        {
            ChangeTextColor(Color.black);

            dataModel.OnConfirm?.Invoke();
        }
        #endregion

        #region Private Methods
        private void ChangeTextColor(Color color) 
        {
            HeaderLabel.color = color;
        }
        #endregion
    }
}